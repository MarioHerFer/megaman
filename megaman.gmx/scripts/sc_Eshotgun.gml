move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10){
vsp+=grav;
}

if(hp_points <= 0){
global.puntos += 100;
instance_destroy(self);
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x+= sign(hsp);
    }
    hsp = 0;
    hcollision = true;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;


//ground collision visto == false
if(visto == false){
if(place_meeting(x,y+8,obj_wall))
{  
    if(fearofheights && !position_meeting(x+(sprite_height/4)*dir,y+(sprite_height/2)+8,obj_wall)){
        dir *= -1;
    }
}

if(dir==-1){
    key_left = -1;
    key_right = 0;
}else{
    key_left = 0;
    key_right = 1;
}

if(hcollision){
    dir = -dir;
    hcollision = false;
}
}

//ground collision visto == true

//-------detectar enemigo
j = obj_camara.x;
l = obj_camara.y;

if((x < j) && (x+500 > j) && (y+150 > l) && (y-150 < l)){
        key_left = 0;
        dir = 1;
        image_xscale = 1;
        visto = true; 
        if(fearofheights && !position_meeting(x+(sprite_height/2)*dir,y+(sprite_height/2)+8,obj_wall)){
        key_right = 0;
            }else{
        key_right = 1;
        }
        if(x+250 > j){
            key_right = 0;
        }
    }else{visto = false;}
    
if((x > j) && (x-500 < j) && (y+150 > l) && (y-150 < l)){
        key_right = 0;
        dir = -1;
        image_xscale = -1;
        visto = true; 
        if(fearofheights && !position_meeting(x+(sprite_height/2)*dir,y+(sprite_height/2)+8,obj_wall)){
        key_left = 0;
        }
        else{
        key_left = -1;
        }
        if(x-250 < j){
            key_left = 0;
        }
    }else{visto = false;
}



//------------------------------animaciones
//quieto
if(hsp == 0){
sprite_index = spr_E_shotgun;
image_speed = 0.05;
}
//derecha
if(hsp > 1){
sprite_index = spr_E_shotgun_run;
image_xscale = 1;
image_speed = 0.13;
}
//izquierda
if(hsp < -1){
sprite_index = spr_E_shotgun_run;
image_xscale = -1;
image_speed = 0.13;
}
